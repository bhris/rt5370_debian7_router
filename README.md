
# A working configuration to run Debian 7 (Wheezy) with the RT5370 as WLAN router

This is a small working configuration to make Debian 7 in combination with the
RT5370 act as WLAN router.

In advance to the setup some packages have to be installed (bridge-utils may
not be needed):

    apt-get install bridge-utils dnsmasq firmware-ralink hostapd

Afterwards the files of this repository should be copied to their destination
on the target file system.

In the last step the names of the WLAN interfaces must be changed to match the
needs of the target system (in this configuration the router is run on *wlan1*
which may not be the case on other systems).
Additionally in this configuration eth0 has attached a static IP address in
/etc/network/interfaces. Most users will want to change the configuration to
use DHCP:

    auto eth0
    iface eth0 inet dhcp
